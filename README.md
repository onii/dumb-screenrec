# Dumb Screen Recorder

***

This script is dumb. It creates a virtual screen with Xvfb,
launches Chrome pointed at a URL and then records the whole thing with ffmpeg.

You *must* make changes to the script before running it, because every setup is
different. I've tried to include helpful comments.
