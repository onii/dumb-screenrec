#!/bin/bash
# Script for recording Netflix
# Using Xvfb, ffmpeg, wmctrl, xdotool, openbox and Chrome

while true; do
    read -p "This is a quick check to make sure 
that you've looked over this script entirely, 
that you've setup a .netflix config file,
that you've correctly set audio output, 
and that you've opened Chrome, signed in to Netflix, and closed Chrome. y/n: " yn
    case $yn in
        [Yy]* ) echo "Okay, let's get started."; break;;
        [Nn]* ) echo "Exiting."; exit;;
        * ) echo "Please answer yes or no.";;
    esac
done

configFile='.netflix'
# Config file should be <min> <res> <url> like:
# 60 1280x720 https://netflix.com/something
while read -r line
do
    sline=($line)
    for i in "${sline[0]}"; do
        min+=("$i")
    done
    for i in "${sline[1]}"; do
        res+=("$i")
    done
    for i in "${sline[2]}"; do
        url+=("$i")
    done
done < "$configFile"
# Loading config file into arrays

for (( i=0; i<${#min[@]}; i++)); do
# For each line (video) in configFile
    echo "Opening everything."
    Xvfb :99 -screen 0 "${res[$i]}"x24 &>/dev/null &
    xvpget=$!
    export DISPLAY=:99
    openbox &>/dev/null &
    sleep 1
    google-chrome --app="${url[$i]}" &>/dev/null &
    gcpget=$!
    sleep 5 # Wait for Chrome to open fully
    winid=`wmctrl -lp | grep "$gcpget" | awk '{print $1}'` # Find Chrome PID
    ##
    # This section needs work to use ${res[i]} in the future
    xdotool windowmove "$winid" 0 0
    xdotool windowsize "$winid" 1280 720
    xdotool mousemove 1400 900 # Get cursor off screen
    echo "Everything open."
    sleep 1
    #
    ##
    
    ## Netflix starts playing
    #
    echo "Waiting for Netflix to start playing."
    sleep 20
    xdotool key f # Fullscreen
    echo "Made window fullscreen."
    sleep 1
    # Go back to video start if necessary
    echo "Seeking to video start."
    for key in {1..25}; do
        xdotool key Left && sleep 1
    done
    sleep 4
    xdotool key space # Press play
    echo "Video playing. Starting recording."
    sleep 1
    #
    ##
    
    # Start recording
    etime=`date +%s`
    filename=`echo "${url[$i]}" | cut -f2 -d\% | sed s/.*[^0-9]//`
    # Extract episode number from URL
    cd ~/Videos # Change if necessary
    # Use pacmd list-sources | grep "alsa_output"
    # to find soundcard
    ffmpeg \
    -loglevel panic \
    -f pulse -ac 2 -ar 44100 -i alsa_output.pci-0000_00_14.2.analog-stereo.monitor \
    -thread_queue_size 64 -f x11grab -framerate 24 -video_size 1280x720 -i :99 \
    -filter_complex asyncts \
    -c:v libx264 -preset superfast -pix_fmt yuv420p \
    -c:a libmp3lame -ac 2 -ar 44100 -b:a 320k \
    Netflix_"$((filename+1))"_"$etime".mkv & # Produces about 600MB/45m @ 720p
    ffpget=$!
    
    ##
    # Giving feedback
    ctime=`date +%H:%M`
    # Current 24h time
    etime=`date +%s`
    # Current Epoch time
    echo "File output: Netflix_$((filename+1))_$etime.mkv"
    echo "Current time: $ctime"
    let "ektime = $etime + (${min[$i]}*60)"
    ktime=`date -d @$ektime +%H:%M`
    echo "Recording for ${min[$i]}m, stopping at $ktime."
    echo "Recording $(($i+1)) / ${#min[@]}."
    sleep "${min[$i]}"m
    #
    ##

    kill "$ffpget"
    kill "$gcpget"
    kill "$xvpget"
    echo "Finished recording."
    echo "----------"
    sleep 15 # Make sure everything is closed
done
